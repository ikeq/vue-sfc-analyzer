import { analyze } from '../src';

describe('sfcs', () => {
  it('parse properly', async () => {
    const res = await analyze('sfcs', {
      ctx: __dirname,
      ignoreTags(tag) {
        return tag === 'ignore-tag';
      },
    });
    expect(res).toMatchSnapshot();
  });
});
