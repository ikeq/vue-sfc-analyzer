import { analyze } from '../src';
import { AnalysisMap } from '../src/parse';

describe('sfcs', () => {
  it('parse properly', async () => {
    const res = await analyze('fallback', {
      ctx: __dirname,
      fallback(template, env) {
        if (!env.path.endsWith('.js')) return null;

        const groups = [{
          matcher: /['"]f(-[a-z]+)+['"]/g
        }, {
          matcher: /['"]fd(-[a-z]+)+['"]/g
        }, {
          matcher: /['"]el(-[a-z]+)+['"]/g
        }, {
          matcher: /['"]a(-[a-z]+)+['"]/g
        }];
        const extProp = '@@fileExt';

        const res = groups.reduce((map, g) => {
          const matched = template.match(g.matcher) || [];

          matched.forEach(i => {
            const t: AnalysisMap[string] = map[i] || {
              total: 0,
              alias: [],
              props: {
                [extProp]: []
              },
            };
            t.total += 1;
            t.props[extProp].push('script');
            map[i] = t;
          });
          return map;
        }, {} as AnalysisMap);

        return res;
      }
    });
    expect(res).toMatchSnapshot();
  });
});
