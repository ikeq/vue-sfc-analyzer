import { readdir, stat } from 'fs/promises';
import { join } from 'path';

export async function walk(base: string, callback: (path: string, meta: { vue: boolean }) => void) {
  const dir = await readdir(base);

  const fn: any = async () => {
    if (!dir.length) return;

    const p = dir.shift() as string;
    const path = join(base, p);
    const fstat = await stat(path);

    if (fstat.isDirectory()) {
      await walk(path, callback);
    } else {
      await callback(path, { vue: path.endsWith('.vue') });
    }

    return fn();
  };

  await fn();
}
