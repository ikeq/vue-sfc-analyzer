import { readFile } from 'fs/promises';
import { join } from 'path';
import { parseFile, ParseOptions, AnalysisMap } from './parse';
import { walk } from './walk';

interface AnalysisResult {
  [tagName: string]: Omit<AnalysisMap[string], 'props'> & {
    files: number;
    props: Array<{
      name: string;
      total: number;
      details: Array<{
        path: string;
        values: string[];
      }>;
    }>;
    [name: string]: any;
  };
}

interface Options extends ParseOptions {
  ctx?: string;
  fallback?: (template: string, env: { path: string }) => AnalysisMap | null;
}

export async function analyze(dir: string, opts: Options): Promise<AnalysisResult> {
  const ctx = opts?.ctx ?? process.cwd();
  const entries: AnalysisResult = {};
  await walk(join(ctx, dir), async (path, meta) => {
    let res = null;
    if (meta.vue) {
      res = await parseFile(path, {
        ignoreTags: opts.ignoreTags,
      });
    } else if (opts.fallback) {
      const template = await readFile(path, 'utf8');
      res = opts.fallback(template, { path });
    }
    if (!res) return;
    // entries[path.slice(base.length)] = res;
    for (const tagName in res) {
      if (!entries[tagName]) {
        entries[tagName] = {
          total: 0,
          files: 0,
          props: [],
          propMap: {},
          alias: [],
        };
      }
      const tag = res[tagName];
      const tt = entries[tagName];
      const ttPropMap = tt.propMap;

      tt.total += tag.total || 1;
      tt.files += 1;
      tt.alias = [...new Set(tt.alias.concat(tag.alias).flat())];

      for (const propName in tag.props) {
        const data = tag.props[propName];
        if (!ttPropMap[propName]) {
          ttPropMap[propName] = {
            name: propName,
            total: 0,
            details: [],
          };
        }

        // insertion
        ttPropMap[propName].total += data.length;
        ttPropMap[propName].details.push({
          path: path.slice(ctx.length),
          values: data.filter((i) => i !== '_empty_'),
        });
      }
    }
  });

  // props sorting
  const getPrefixWeight = (prop: string) => {
    if (/^v-/.test(prop)) return 1;
    if (/^:/.test(prop)) return 2;
    if (/^[a-zA-Z]/.test(prop)) return 3;
    if (/^#/.test(prop)) return 4;
    if (/^@/.test(prop)) return 5;
    if (/^_/.test(prop)) return 6;
    return 0;
  };
  for (const tagName in entries) {
    const tag = entries[tagName];
    tag.props = Object.values<AnalysisResult[string]['props'][number]>(tag.propMap).sort((a, b) => {
      const aw = getPrefixWeight(a.name);
      const bw = getPrefixWeight(b.name);
      if (aw === bw) {
        return a.name > b.name ? 1 : -1;
      }
      return aw - bw;
    }) as any;
    delete tag.propMap;
  }

  return entries;
}
