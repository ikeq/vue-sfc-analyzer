import { ASTElement, compile } from 'vue-template-compiler';
import kebabCase from 'lodash/kebabCase';
import camelCase from 'lodash/camelCase';

const htmlTags = [
  'a',
  'abbr',
  'address',
  'area',
  'article',
  'aside',
  'audio',
  'b',
  'base',
  'bdi',
  'bdo',
  'blockquote',
  'body',
  'br',
  'button',
  'canvas',
  'caption',
  'cite',
  'code',
  'col',
  'colgroup',
  'data',
  'datalist',
  'dd',
  'del',
  'details',
  'dfn',
  'dialog',
  'dir',
  'div',
  'dl',
  'dt',
  'em',
  'embed',
  'fieldset',
  'figcaption',
  'figure',
  'font',
  'footer',
  'form',
  'frame',
  'frameset',
  'h1',
  'h2',
  'h3',
  'h4',
  'h5',
  'h6',
  'head',
  'header',
  'hgroup',
  'hr',
  'html',
  'i',
  'iframe',
  'img',
  'input',
  'ins',
  'kbd',
  'label',
  'legend',
  'li',
  'link',
  'main',
  'map',
  'mark',
  'marquee',
  'menu',
  'meta',
  'meter',
  'nav',
  'noscript',
  'object',
  'ol',
  'optgroup',
  'option',
  'output',
  'p',
  'param',
  'picture',
  'pre',
  'progress',
  'q',
  'rp',
  'rt',
  'ruby',
  's',
  'samp',
  'script',
  'section',
  'select',
  'slot',
  'small',
  'source',
  'span',
  'strong',
  'style',
  'sub',
  'summary',
  'sup',
  'table',
  'tbody',
  'td',
  'template',
  'textarea',
  'tfoot',
  'th',
  'thead',
  'time',
  'title',
  'tr',
  'track',
  'u',
  'ul',
  'var',
  'video',
  'wbr',
];

const rSync = /\.sync$/;

export interface AnalysisMap {
  [tag: string]: {
    total: number;
    alias: string[];
    props: {
      [prop: string]: any[];
    };
  };
}
export interface ParseOptions {
  ignoreTags?: string[] | ((tag: string) => void);
}

export default function (code: string, options?: ParseOptions) {
  const ignoreTags = (() => {
    const igTags = options?.ignoreTags;

    if (typeof igTags === 'function') return igTags;
    if (Array.isArray(igTags)) {
      return (tag: string) => igTags.includes(tag);
    }
    return () => false;
  })() as any;
  const { ast } = compile(code);

  if (!ast?.children?.[0]) return;

  const ret = parse(ast.children[0] as any);

  // omit ignore tags
  for (const tag in ret) {
    if (ignoreTags(tag)) {
      delete ret[tag];
    }
  }

  return ret;

  function parse(
    ast: ASTElement,
    map: AnalysisMap = {},
    opts?: { abstract: boolean }
  ): AnalysisMap {
    if (ast.ifConditions) {
      ast.ifConditions.forEach((astIf) => {
        astIf.block.ifConditions = undefined;
        parse(astIf.block, map);
      });
      return map;
    }
    // parse component tag earlier
    if (ast.component) {
      return parse(
        {
          ...ast,
          tag: ast.component,
          component: undefined,
        },
        map
      );
    }

    if (!ast.tag) return map;

    // forward slots
    for (const slotName in ast.scopedSlots) {
      const slot = ast.scopedSlots[slotName];
      const slotAsProp = `#${slotName.replace(/"/g, '')}`;
      parse(
        {
          tag: ast.tag,
          attrsList: [
            {
              name: slotAsProp,
              value: slot.slotScope,
            },
          ],
        } as ASTElement,
        map,
        { abstract: true }
      );
      // merge <template #slot> children into parent
      ast.children.push(...(ast.scopedSlots as any)[slotName].children);
    }
    // <div slot="xxx">
    if (ast.slotTarget) {
      parse(
        {
          tag: ast.parent?.tag,
          attrsList: [
            {
              name: `#${ast.slotTarget.replace(/"/g, '')}`,
            },
          ],
        } as ASTElement,
        map,
        { abstract: true }
      );
    }

    // parse start
    const tag = (() => {
      if (/^[hH]\d$/.test(ast.tag)) return ast.tag.toLowerCase();
      if (/[\.']/.test(ast.tag)) return ast.tag;
      return kebabCase(ast.tag);
    })();

    if (!map[tag]) {
      map[tag] = {
        total: 0,
        props: {},
        alias: [],
      };
    }
    const tagMap = map[tag];

    if (ast.tag !== tag && !tagMap.alias?.includes(ast.tag)) {
      tagMap.alias.push(ast.tag);
    }

    if (!opts?.abstract) {
      tagMap.total += 1;
    }

    // normal props
    (ast.attrsList.length ? ast.attrsList : [{ name: '_no_props_', value: undefined }]).forEach(
      (attr) => {
        // escape v-on:[name] which will be processed below
        if (/^(v-on:|@)/.test(attr.name)) return;
        if (!tagMap.props[attr.name]) {
          tagMap.props[attr.name] = [];
        }
        tagMap.props[attr.name].push(
          attr.value !== undefined ? JSON.stringify(attr.value) : '_empty_'
        );
      }
    );
    ast.children?.forEach((child) => {
      parse(child as any, map);
    });

    // events
    const syncPropMap = ast.attrsList.reduce((map, attr) => {
      if (!rSync.test(attr.name)) return map;
      const name = attr.name.slice(1, -5);
      const camelcasedName = camelCase(name);
      const data = {
        event: `update:${name}`,
        value: `${attr.value}=$event`,
      };
      map[data.event] = data;
      map[`update:${camelcasedName}`] = Object.assign({ derived: name !== camelcasedName }, data);
      return map;
    }, {} as Record<string, { event: string; value: string; derived?: boolean }>);

    for (const event in ast.events) {
      const ev = `@${event}`;
      const handlers = ast.events[event];
      if (syncPropMap[event]) {
        // when prop has `.sync`, vue-template-compiler will parse its kebab cased name
        // to camel case additionally
        if (syncPropMap[event].derived) {
          // remove the additional handler
          const syncProp = syncPropMap[event];
          const syncHandlers = ast.events[syncProp.event];
          if (Array.isArray(syncHandlers)) {
            const index = syncHandlers.findIndex((i) => i.value === syncProp.value);
            if (~index) {
              syncHandlers.splice(index, 1);
            }
          }
          continue;
        }
      }
      if (!tagMap.props[ev]) {
        tagMap.props[ev] = [];
      }
      tagMap.props[ev].push(...(Array.isArray(handlers) ? handlers : [handlers]));
    }

    // parse #default for non-w3c tags
    if (!htmlTags.includes(tag) && ast.children?.length) {
      if (!tagMap.props['#default']) {
        tagMap.props['#default'] = [];
      }
      tagMap.props['#default'].push(ast.children.length);
    }

    return map;
  }
}
