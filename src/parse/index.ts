import { readFile } from 'fs/promises';
import parseAst, { ParseOptions, AnalysisMap } from './ast';

export function parse(template: string, parseOptions: ParseOptions) {
  return parseAst(template, parseOptions);
}

export async function parseFile(path: string, parseOptions: ParseOptions) {
  try {
    const template = await readFile(path, 'utf8');
    return parse(template, parseOptions);
  } catch (e) {
    return Promise.reject(e);
  }
}

export { ParseOptions, AnalysisMap };
